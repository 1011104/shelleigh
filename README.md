## CVE-2019-1069 - Windows Task Scheduler LPE.

This vulnerability resides in the Windows Task Scheduler process.

Allow to run a malformed **.job** file that exploits a flaw in the way the Task Scheduler process changes DACL (Discretionary Access Control List) permissions for an individual file.
When exploited, this vulnerability can elevate a low-privileged account to admin access.

BearLPE by SandboxEscaper

## Examples

- C:\> exploit.exe **\<user\>**  **\<password\>**  **\<file\>**
- C:\> exploit.exe limited Limited123- C:\Windows\System32\sethc.exe

<div align="center">
  <img src="https://gitlab.com/kash_/cve-2019-1069/raw/master/poc/poc.png" alt="POC"/>
</div>
